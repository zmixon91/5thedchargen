(function () {
	'use strict';
	angular
		.module('app')
		.component('myhome', {
			bindings: {
				count: '='
			},
			controller: 'homeCtrl as ctrl',
			templateUrl: 'js/components/home/home.html'
		});
})();