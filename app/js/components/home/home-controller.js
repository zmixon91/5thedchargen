(function () {
	'use strict';
	angular
		.module('app')
		.controller('homeCtrl', homeCtrl);

	function homeCtrl($scope, DataService, CharacterService) {

		// Vars
		var vm = this;
		var backgrounds = {};
		var races = {};
		vm.backgrounds = {};
		vm.races = {};
		vm.selectionBg = "Random";
		vm.selectionRa = "Random";
		vm.selectionCl = "Random";
		vm.regenerate = regenerate;

		DataService.getData().then(function (data) {
			vm.backgrounds = data.backgrounds;
			vm.races = data.races;
			vm.classes = data.classes;
			$scope.$apply(vm.regenerate());
		});

		// Functions
		function regenerate(selectionBackground, selectionRace, selectionClass) {
			vm.char = CharacterService.buildChar(selectionBackground, selectionRace, selectionClass);
		}

	}

})();