(function () {
	'use strict';
	angular
		.module('app')
		.service('DataService', DataService);

	function DataService($http) {

		var vm = this;

		vm.getBackgrounds = getBackgrounds;
		vm.getData = getData;
		vm.getRaces = getRaces;
		vm.getClasses = getClasses;

		function getBackgrounds() {
			return $http.get('js/json/backgrounds.json').then(function (res) {
				return {
					backgrounds: Object.keys(res.data),
					backgroundData: res.data
				};
			});
		}

		function getRaces() {
			return $http.get('js/json/races.json').then(function (res) {
				return {
					races: Object.keys(res.data),
					raceData: res.data
				};
			});
		}

		function getClasses() {
			return $http.get('js/json/classes.json').then(function (res) {
				return {
					classes: Object.keys(res.data),
					classData: res.data
				};
			});
		}

		function getData() {
			return Promise.all([vm.getBackgrounds(), vm.getRaces(), vm.getClasses()]).then(function (data) {
				var out = Object.assign({}, data[0], data[1], data[2]);
				return out;
			});
		}

	}

})();