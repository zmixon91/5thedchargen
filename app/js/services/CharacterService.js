(function () {
	'use strict';
	angular
		.module('app')
		.service('CharacterService', CharacterService);

	function CharacterService(DataService) {

		var vm = this;
		var backgrounds = null;
		var races = null;
		var classes = null;
		vm.buildChar = buildChar;
		var data = DataService.getData().then(function (data) {
			backgrounds = data.backgroundData;
			races = data.raceData;
			classes = data.classData;
		});

		function buildChar(background, race, myClass) {

			// Parse input
			if (!background || background === "Random") {
				var bgs = Object.keys(backgrounds);
				bgs.shift();
				background = chance.pickone(bgs);
			}
			if (!race || race === "Random") {
				var ras = Object.keys(races);
				ras.shift();
				race = chance.pickone(ras);
			}
			if (!myClass || myClass === "Random") {
				var cls = Object.keys(classes);
				cls.shift();
				myClass = chance.pickone(cls);
			}

			// Vars
			var out = {};
			var bgd = null;
			var rad = null;
			var cld = null;
			var outGen = null;
			var outBg = null;
			var outRa = null;
			var outCl = null;
			var outCon = null;
			bgd = backgrounds[background];
			rad = races[race];
			cld = classes[myClass];

			// Build output
			out = Object.assign({}, charGen(), charBg(background, bgd), charRa(race, rad), charCl(myClass, cld), charCon(bgd, rad));

			return out;
		}

		function charGen() {

			// Vars
			var out = {};

			// Assign data
			out.gender = chance.gender();
			out.name = chance.name({ middle_initial: true, gender: out.gender });

			return out;
		}

		function charCl(myClass, classData) {

			// Vars
			var out = {};

			// Assign data
			out.class = myClass;

			return out;
		}

		function charBg(background, backgroundData) {

			// Vars
			var out = {};

			// Assign data
			out.background = background;
			out.feature = backgroundData.feature;
			out.trait = chance.pickone(backgroundData.traits);
			out.ideal = chance.pickone(backgroundData.ideals);
			out.bond = chance.pickone(backgroundData.bonds);
			out.flaw = chance.pickone(backgroundData.flaws);
			out.skills = backgroundData.skills;
			out.equipment = backgroundData.equipment;
			if (backgroundData.extra) out.specialty = [backgroundData.extra[0], backgroundData.extra[chance.natural({ min: 1, max: backgroundData.extra.length - 1 })]];

			return out;
		}

		function charRa(race, raceData) {

			// Vars
			var out = {};

			// Assign data
			out.race = race;
			out.speed = raceData.speed;
			out.radesc = raceData.desc;
			out.racials = raceData.racials;
			out.ability = raceData.ability;
			out.age = chance.natural({ min: raceData.age[0], max: (raceData.age[1] - raceData.age[1] * 0.4) });
			out.ageMin = raceData.age[0];
			out.ageMax = raceData.age[1];
			if (raceData.sub) {
				var subi = chance.pickone(Object.keys(raceData.sub));
				var subd = raceData.sub[subi];
				out.sub = subi;
				out.radesc += " " + subd.desc;
				out.racials = Object.assign({}, raceData.racials, subd.racials);
				out.ability = Object.assign({}, raceData.ability, subd.ability);
			}

			return out;
		}

		function charCon(backgroundData, raceData) {

			// Vars
			var out = {};

			// Assign data
			out.tools = raceData.tools.concat(backgroundData.tools);
			out.languages = raceData.languages.concat(backgroundData.languages);

			return out;
		}

	}

})();