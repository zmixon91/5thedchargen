var gulp = require('gulp');
var stylus = require('gulp-stylus');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var ngmin = require('gulp-ng-annotate');
var runSequence = require('run-sequence');
var del = require('del');
var minifyhtml = require('gulp-htmlmin');
var minifycss = require('gulp-clean-css');

gulp.task('compileStylus', function () {
	return gulp.src('app/stylus/**/*.styl')
		.pipe(stylus())
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('compilePug', function () {
	return gulp.src('app/**/*.pug')
		.pipe(pug())
		.pipe(gulp.dest('app'));
});

gulp.task('browserSync', function () {
	browserSync.init({
		server: {
			baseDir: 'app'
		},
	});
});

gulp.task('watch', ['browserSync', 'compileStylus', 'compilePug'], function () {
	gulp.watch('app/stylus/**/*.styl', ['compileStylus']);
	gulp.watch('app/**/*.pug', ['compilePug']);
	gulp.watch('app/**/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('default', ['watch'], function () {
	console.log("////  Starting project...  ////");
});

gulp.task('buildJS', function () {
	gulp.src('app/js/**/*.js')
		.pipe(concat('script.js'))
		.pipe(ngmin())
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(rename({ extname: ".min.js" }))
		.pipe(gulp.dest('dist'));
});

gulp.task('buildHTML', ['compilePug'], function () {
	gulp.src('app/**/*.html')
		.pipe(minifyhtml({collapseWhitespace: true}))
		.pipe(gulp.dest('dist'));
});

gulp.task('buildCSS', ['compileStylus'], function () {
	gulp.src('app/css/*.css')
		.pipe(minifycss())
		.pipe(gulp.dest('dist'));
});

gulp.task('buildClean', function () {
	return del('dist');
});

gulp.task('buildCopy', function () {
	gulp.src('app/js/json/*.json')
		.pipe(gulp.dest('dist/js/json'));
});

gulp.task('build', function (callback) {
	runSequence(
		'buildClean',
		['buildHTML', 'buildCSS'],
		'buildJS',
		'buildCopy',
		callback);
});